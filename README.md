## Rust Learning Project

We will be making a simple server that indexes memes. This server will have the following features:

- Scan a folder for images
- Serve those images to a user
- Handle uploading new images to the folder
- Have an endpoint for serving a paginated list of all images
- Have an admin account that can delete images

## Weekly Plan

**Week 1 - Research**
- Pick a Rust Server Stack
- Write a simple server with a hello world endpoint
- Make a gitlab repo for your project and push your code there

**Week 2 - Database**
- Pick a rust DB library
- Make an API that inserts a record into to the DB
- Bonus points - handle db migrations

**Week 3 - Configuration**
- Pick a library to handle your configuration
- Write an API endpoint that prints your configuration
- Bonus Points - write an API endpoint that modifies your configuration

**Week 4 - Project cleanup**
- Start separating different parts of your code into different files
- This can be harder that it sounds

**Week 5 - Error Handling**
- Read up un rust error handling
- Write an api endpoint that sends an image file to the user. Have it handle an error where the file does not exist

**Week 6 - JSON Parsing**
- write an endpoint that parses a JSON body
- HAve your endpoint handle default values and missing values

**Week 7 - Threading**
- Write an api endpoint that kicks off another thread
- Have your thread scan a folder for images
- Add these images to your DB

**Week 8 - Recursion**
- Have your threaded scan function recursively scan a folder for images

**Week 9/10 - Auth**
- Have a login endpoint that can send a JWT token on successful login
    - You only need one account and your can hardcode your username and password
- Build a piece of middleware which can validate your JWT
- Make a delete image endpoint which requires this JWT auth

**Week 11 - Serving images and web pages**
- Give your server the functionality to serve a login page and image view page

**Week 12 - File Upload**
- Make a file upload API to add new memes

**Week 13+ - Pagination**
- Make a pagination endpoint that allows your user to index a list of all the memes
